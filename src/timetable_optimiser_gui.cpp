#include <vector>

#include "stdafx.h"
#include "gfx_func.h"
#include "strings_func.h"
#include "window_func.h"
#include "window_gui.h"
#include "textbuf_gui.h"
#include "widget_type.h"
#include "widgets/timetable_optimiser_widget.h"
#include "table/strings.h"
#include "vehiclelist.h"
#include "vehicle_type.h"
#include "vehicle_base.h"
#include "progress.h"
#include "thread/thread.h"
#include "timetable_optimiser.h"
#include "timetable_optimiser_gui.h"
#include "error.h"

bool ProgressCallback(bool feasible_found, void *param);


/** Widgets for the Optimizer window. */
static const NWidgetPart _nested_optimiser_widgets[] = {
  NWidget(NWID_HORIZONTAL),
    NWidget(WWT_CLOSEBOX, COLOUR_GREY),
    NWidget(WWT_CAPTION, COLOUR_GREY, WID_TTO_CAPTION), SetDataTip(STR_TIMETABLE_OPTIMISER_CAPTION, STR_TOOLTIP_WINDOW_TITLE_DRAG_THIS),
  EndContainer(),
  NWidget(WWT_PANEL, COLOUR_GREY), SetPIP(0, 0, 0),
    NWidget(NWID_VERTICAL), SetPIP(7, 7, 7),
      NWidget(NWID_VERTICAL), SetPIP(0, 0, 0),
	NWidget(NWID_HORIZONTAL), SetPIP(7, 0, 7),
	  NWidget(WWT_PUSHARROWBTN, COLOUR_YELLOW, WID_TTO_PERIOD_DECREASE), SetFill(0, 1), SetDataTip(AWV_DECREASE, STR_NULL),
	  NWidget(WWT_PUSHARROWBTN, COLOUR_YELLOW, WID_TTO_PERIOD_INCREASE), SetFill(0, 1), SetDataTip(AWV_INCREASE, STR_NULL),
	  NWidget(NWID_SPACER), SetMinimalSize(6, 0),
	  NWidget(WWT_TEXT, COLOUR_MAUVE, WID_TTO_PERIOD), SetDataTip(STR_TIMETABLE_OPTIMISER_PERIOD, STR_NULL), SetFill(1, 0), SetPadding(1, 0, 0, 0),
	EndContainer(),
	NWidget(NWID_HORIZONTAL), SetPIP(7, 0, 7),
	  NWidget(WWT_PUSHARROWBTN, COLOUR_YELLOW, WID_TTO_STATION_WAIT_DECREASE), SetFill(0, 1), SetDataTip(AWV_DECREASE, STR_NULL),
	  NWidget(WWT_PUSHARROWBTN, COLOUR_YELLOW, WID_TTO_STATION_WAIT_INCREASE), SetFill(0, 1), SetDataTip(AWV_INCREASE, STR_NULL),
	  NWidget(NWID_SPACER), SetMinimalSize(6, 0),
	  NWidget(WWT_TEXT, COLOUR_MAUVE, WID_TTO_STATION_WAIT), SetDataTip(STR_TIMETABLE_OPTIMISER_STATION_WAIT, STR_NULL), SetFill(1, 0), SetPadding(1, 0, 0, 0),
	EndContainer(),
	NWidget(NWID_HORIZONTAL), SetPIP(7, 0, 7),
	  NWidget(WWT_PUSHARROWBTN, COLOUR_YELLOW, WID_TTO_SEPARATION_DECREASE), SetFill(0, 1), SetDataTip(AWV_DECREASE, STR_NULL),
	  NWidget(WWT_PUSHARROWBTN, COLOUR_YELLOW, WID_TTO_SEPARATION_INCREASE), SetFill(0, 1), SetDataTip(AWV_INCREASE, STR_NULL),
	  NWidget(NWID_SPACER), SetMinimalSize(6, 0),
	  NWidget(WWT_TEXT, COLOUR_MAUVE, WID_TTO_SEPARATION), SetDataTip(STR_TIMETABLE_OPTIMISER_SEPARATION, STR_NULL), SetFill(1, 0), SetPadding(1, 0, 0, 0),
	EndContainer(),
      EndContainer(),
      NWidget(NWID_HORIZONTAL), SetPIP(7, 0, 7),
	NWidget(WWT_PUSHTXTBTN, COLOUR_YELLOW, WID_TTO_EXECUTE), SetResize(0, 0), SetFill(0, 0), SetDataTip(STR_TIMETABLE_OPTIMISER_EXECUTE, STR_TIMETABLE_OPTIMISER_EXECUTE_TOOLTIP),
      EndContainer(),
    EndContainer(),
  EndContainer(),
};

static const NWidgetPart _nested_optimiser_progress_widgets[] = {
  NWidget(WWT_CAPTION, COLOUR_GREY), SetDataTip(STR_TIMETABLE_OPTIMISER_CAPTION, STR_TOOLTIP_WINDOW_TITLE_DRAG_THIS),
  NWidget(WWT_PANEL, COLOUR_GREY),
    NWidget(NWID_HORIZONTAL), SetPIP(20, 0, 20),
      NWidget(NWID_VERTICAL), SetPIP(11, 8, 11),
	NWidget(WWT_LABEL, INVALID_COLOUR), SetDataTip(STR_TIMETABLE_OPTIMISER_BUSY_MESSAGE, STR_NULL), SetFill(1, 0),
	NWidget(WWT_EMPTY, INVALID_COLOUR, WID_TTO_PROGRESS_TEXT), SetMinimalSize(40, 40), SetFill(1, 0),
	NWidget(WWT_PUSHTXTBTN, COLOUR_YELLOW, WID_TTO_CANCEL), SetResize(0, 0), SetFill(1,0), SetDataTip(STR_TIMETABLE_OPTIMISER_CANCEL, STR_NULL),
      EndContainer(),
    EndContainer(),
  EndContainer(),
};

static const WindowDesc _optimiser_desc(
  WDP_CENTER, 0, 0,
  WC_TIMETABLE_OPTIMISER, WC_NONE,
  0,
  _nested_optimiser_widgets, lengthof(_nested_optimiser_widgets)
);

static const WindowDesc _optimiser_progress_desc (
  WDP_CENTER, 0, 0,
  WC_TIMETABLE_OPTIMISER_PROGRESS, WC_NONE,
  0,
  _nested_optimiser_progress_widgets, lengthof(_nested_optimiser_progress_widgets)
);

TimetableProgressWindow::TimetableProgressWindow(const WindowDesc *desc, TimetableOptimiserWindow *parent) :
  Window(),
  m_runtime(0), m_feasible_found(false),
  m_parent(parent),
  m_optimiser_done(false), m_optimiser_successful(false), m_optimiser_optimal(false), m_must_cancel(false), m_querying(false)
{
  this->Init(desc);
}
  
TimetableProgressWindow::~TimetableProgressWindow() {
    // Window closed
    m_optimiser_status_mutex->BeginCritical();

    if ( ! m_optimiser_done ) {
      this->m_must_cancel = true;
    }
    
    m_optimiser_status_mutex->EndCritical();
    
    if ( m_parent->thread ) {
      m_parent->thread->Join();
      m_parent->thread = NULL;
    }
    
    m_parent->progress_window = NULL;
    
    delete m_optimiser_status_mutex;
    
    m_optimiser_status_mutex = NULL;
}

void TimetableProgressWindow::Init(const WindowDesc *desc) {
  this->parent = m_parent;

  this->CreateNestedTree(desc);
  this->FinishInitNested(desc);
 
  m_optimiser_status_mutex = ThreadMutex::New();
}

void TimetableProgressWindow::DrawWidget(const Rect& r, int widget) const {
  m_optimiser_status_mutex->BeginCritical();
  
  switch (widget) {
    case  WID_TTO_PROGRESS_TEXT:
      char buffer[16];
      snprintf(buffer, sizeof(buffer), "%u:%02u", m_runtime / 60, m_runtime % 60 );
      
      int y = r.top;
      
      if ( m_must_cancel ) {
	DrawString(r.left, r.right, y, STR_TIMETABLE_OPTIMISER_STATUS_CANCELLING);
      } else if (this->m_feasible_found ) {
	DrawString(r.left, r.right, y, STR_TIMETABLE_OPTIMISER_STATUS_OPTIMISING);
      } else {
	DrawString(r.left, r.right, y, STR_TIMETABLE_OPTIMISER_STATUS_SEARCHING);
      }
      
      y += FONT_HEIGHT_NORMAL;
      
      SetDParamStr(0, buffer);
      DrawString(r.left, r.right, y, STR_TIMETABLE_OPTIMISER_RUNNING_TIME);
      
      break;
  }
  
  m_optimiser_status_mutex->EndCritical();
}

void TimetableProgressWindow::OnOptimisationComplete (bool successful, bool optimal) {
  m_optimiser_status_mutex->BeginCritical();

  m_optimiser_done = true;
  m_optimiser_successful = successful;
  m_optimiser_optimal = optimal;
  
  m_optimiser_status_mutex->EndCritical();
}


static void ConfirmApplyCallback(Window *w, bool confirmed)
{
  TimetableOptimiserWindow *pw = static_cast<TimetableOptimiserWindow*>(w);
  
  if ( confirmed ) {
    pw->optimiser->ApplyTimetables();
  }

  delete pw->optimiser;
  pw->optimiser = NULL;
}

void TimetableProgressWindow::OnHundredthTick() {
  if ( !m_querying ) { 
    // We use the main thread to apply the time tables.
    m_optimiser_status_mutex->BeginCritical();

    if ( m_optimiser_done) {
      if ( m_optimiser_successful ) {
	m_querying = true;
	ShowQuery(
	  STR_TIMETABLE_SOLUTION_DIALOG_CAPTION_FOUND,
	  m_optimiser_optimal ? STR_TIMETABLE_OPTIMISER_CONFIRM_OPTIMAL_SOLUTION : STR_TIMETABLE_OPTIMISER_CONFIRM_SUBOPTIMAL_SOLUTION,
	  m_parent,
	  ConfirmApplyCallback
	);
	
      } else {
	ShowErrorMessage(STR_TIMETABLE_OPTIMISER_INFORM_INFEASIBLE, INVALID_STRING_ID, WL_WARNING);
      }
    } else {
      this->m_runtime = m_parent->optimiser->GetElapsedTime();
      this->InvalidateData();
    }
    
    m_optimiser_status_mutex->EndCritical();
    
    if (m_optimiser_done) {
      delete this;
    }
  }
};

void TimetableProgressWindow::OnClick(Point pt, int widget, int click_count) {
  switch (widget) {
    case WID_TTO_CANCEL:
      this->Cancel();
      break;
  }
}

bool TimetableProgressWindow::UpdateProgress(bool feasible_found) {
  m_optimiser_status_mutex->BeginCritical();
  bool will_cancel = m_must_cancel;
  m_feasible_found = feasible_found;
  this->m_runtime = m_parent->optimiser->GetElapsedTime();
  this->InvalidateData();
  m_optimiser_status_mutex->EndCritical();
  return !will_cancel;
}

void TimetableProgressWindow::Cancel()
{
  m_optimiser_status_mutex->BeginCritical();
  m_must_cancel = true;
  this->InvalidateData();
  m_optimiser_status_mutex->EndCritical();
}

bool ProgressCallback(bool feasible_found, void *param) {
  TimetableProgressWindow *w = static_cast<TimetableProgressWindow*>(param);
  return w->UpdateProgress(feasible_found);
}

void StartOptimiser (void *param) {
  TimetableProgressWindow *w = static_cast<TimetableProgressWindow*>(param);

  SolveResult res = w->m_parent->optimiser->OptimiseTimetables();
  
  if ( res == SolveResult::OPTIMAL || res == SolveResult::FEASIBLE ) {
    w->OnOptimisationComplete(true, res == SolveResult::OPTIMAL);
  } else {
    w->OnOptimisationComplete(false, false);
  }

}

TimetableOptimiserWindow::TimetableOptimiserWindow(const WindowDesc *desc, VehicleType vtype) :
    Window(), thread(NULL), vlid(NULL), vtype(vtype), progress_window(NULL), optimiser(NULL) {
  this->optimiser_parms = new TimetableOptimiserParameters();
  this->CreateNestedTree(desc);
  this->FinishInitNested(desc);
}

TimetableOptimiserWindow::TimetableOptimiserWindow(const WindowDesc *desc, const VehicleListIdentifier &vlid) :
    Window (), thread(NULL), vlid(&vlid), vtype(vlid.vtype), progress_window(NULL), optimiser(NULL) {
  this->optimiser_parms = new TimetableOptimiserParameters();
  this->CreateNestedTree(desc);
  this->FinishInitNested(desc);  
}

TimetableOptimiserWindow::~TimetableOptimiserWindow() {
  if ( progress_window ) {
    // Cancel running jobs.
    progress_window->Cancel();
  }
  
  // Wait until the thread is done.
  if ( thread ) {
    thread->Join();
    delete thread;
    thread = NULL;
  }
  
  delete this->optimiser_parms;
  
  if ( optimiser ) {
    delete optimiser;
  }
}

void TimetableOptimiserWindow::OnOptimisationDone()
{
  // Thread will exit next
  thread->Join();
  thread = NULL;
}

void TimetableOptimiserWindow::SetStringParameters(int widget) const {
  switch (widget) {
    case WID_TTO_PERIOD:
      SetDParam(0, optimiser_parms->period);
      break;
    case WID_TTO_STATION_WAIT:
      SetDParam(0, optimiser_parms->min_station_wait);
      break;
    case WID_TTO_SEPARATION:
      SetDParam(0, optimiser_parms->min_separation);
      break;
  }
}

void TimetableOptimiserWindow::Execute() {
  DeleteWindowByClass(WC_TIMETABLE_OPTIMISER_PROGRESS);
  
  progress_window = new TimetableProgressWindow(&_optimiser_progress_desc, this);
  
  optimiser = new TimetableOptimiser(ProgressCallback, *optimiser_parms, progress_window);
  optimiser->RetrieveTimetables(this->vtype, this->vlid);
  
  
  
  ThreadObject::New(StartOptimiser, static_cast<void*>(progress_window), &thread);
}

void TimetableOptimiserWindow::OnClick(Point pt, int widget, int click_count)
{
  switch (widget) {
    case WID_TTO_EXECUTE:
      /* Allow only one optimiser at a time */
      if ( thread == NULL ) { 
	this->Execute();
      }
      break;
    case WID_TTO_PERIOD_INCREASE:
      this->optimiser_parms->period++;
      this->InvalidateData();
      break;
    case WID_TTO_PERIOD_DECREASE: 
      this->optimiser_parms->period = max(1, this->optimiser_parms->period-1);
      this->InvalidateData();
      break;
    case WID_TTO_SEPARATION_INCREASE:
      this->optimiser_parms->min_separation++;
      this->InvalidateData();
      break;
    case WID_TTO_SEPARATION_DECREASE:
      this->optimiser_parms->min_separation = max(1, this->optimiser_parms->min_separation-1);
      this->InvalidateData();
      break;
    case WID_TTO_STATION_WAIT_INCREASE:
      this->optimiser_parms->min_station_wait++;
      this->InvalidateData();
      break;
    case WID_TTO_STATION_WAIT_DECREASE:
      this->optimiser_parms->min_station_wait = max(1, this->optimiser_parms->min_station_wait-1);
      this->InvalidateData();
      break;
  }
}

void ShowTimetableOptimiser(VehicleType vtype) {
  DeleteWindowByClass(WC_TIMETABLE_OPTIMISER);
  new TimetableOptimiserWindow(&_optimiser_desc, vtype);
}

void ShowTimetableOptimiser(const VehicleListIdentifier &vlid) {
  DeleteWindowByClass(WC_TIMETABLE_OPTIMISER);
  new TimetableOptimiserWindow(&_optimiser_desc, vlid);
}