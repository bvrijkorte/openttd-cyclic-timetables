#ifndef TIMETABLE_OPTIMISER_HELPER_H
#define TIMETABLE_OPTIMISER_HELPER_H


enum DestinationType {
  STATION,
  DEPOT,
  WAYPOINT,
};

struct Destination {
  DestinationType type;
  DestinationID id;
  
  bool operator== (const Destination& other) const {
    return ( other.id == this->id && other.type == this->type);
  };
};

struct Movement {
  Destination from;
  Destination to;
  OrderID order_id;
  int order_sequence; /// One-based sequence number of appearance in the vehicle timetable.
  uint16 travel_time;
  uint16 wait_time;
  bool is_end_of_line; /// If the unload all flag is set then we assume that the vehicle is at the line's end.
  bool is_via_order;
};

typedef std::vector<Movement> MovementVector;
typedef std::map<VehicleID, int > TimetableStartTimeMap;

struct Timetable {
  MovementVector movements;
  int start_offset;
  int vehicle_count;
  int total_duration;
};

typedef std::map<VehicleID, Timetable> TimetableMap;

class TimetableOptimiserHelper {
  
  public:
    TimetableOptimiserHelper(TimetableOptimiser *wrapper);
    TimetableOptimiserHelper( const TimetableOptimiserHelper &other );
    
    time_t m_starttime;
    time_t m_last_progress_update;
    
    bool m_found_feasible;
    TimetableOptimiser *m_wrapper;
    TimetableMap m_current_timetables;
    TimetableMap m_optimized_timetables;
    
    DestinationType GetDestinationType(const Order *o) const;
    uint32 ChangeTimetableArg (VehicleID v, uint order_number, bool wait_time) const;
    SolveResult Solve(const TimetableOptimiserParameters &parms);
    
    time_t GetElapsedTime();
    
    void RetrieveTimetables(VehicleType vtype, const VehicleListIdentifier *vlid = NULL);
};

#endif
