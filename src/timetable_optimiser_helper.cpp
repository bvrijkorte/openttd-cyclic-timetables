#include "stdafx.h"

#include <glpk.h>
#include <time.h>

#include <utility>
#include <vector>
#include <map>

#include "command_func.h"
#include "date_func.h"
#include "vehicle_base.h"
#include "vehiclelist.h"
#include "group.h"

#include "timetable_optimiser.h"
#include "timetable_optimiser_helper.h"

TimetableOptimiserHelper::TimetableOptimiserHelper( const TimetableOptimiserHelper &other ) :
  m_starttime( other.m_starttime ), m_last_progress_update( other.m_last_progress_update ), m_found_feasible( other.m_found_feasible ),
  m_wrapper( other.m_wrapper ), m_current_timetables( other.m_current_timetables ), m_optimized_timetables( other.m_optimized_timetables )
{
}

DestinationType TimetableOptimiserHelper::GetDestinationType(const Order *o) const {
  switch (o->GetType()) {
    case  OT_GOTO_STATION:
      return DestinationType::STATION;
    case OT_GOTO_DEPOT:
      return DestinationType::DEPOT;
    case OT_GOTO_WAYPOINT:
      return DestinationType::WAYPOINT;
    default:
      assert(false); // Unknown destination type?
  }
  
  return DestinationType::STATION;
}

void glpk_callback(glp_tree *tree, void *info) {
  TimetableOptimiserHelper *helper = static_cast<TimetableOptimiserHelper*>(info);
  
  bool must_send_notification = false;
  
  int reason = glp_ios_reason(tree);
  switch (reason) {
    case GLP_IBINGO:
      // Feasible solution found.
      helper->m_found_feasible = true;
      must_send_notification = true;
      break;
    default:
      break;
  }
  
  /* Avoid spamming the GUI with events requiring mutex synchronisation */
  if ( helper->GetElapsedTime() - helper->m_last_progress_update >= 1 ) {
    must_send_notification = true;
  }

  if ( must_send_notification ) {
    helper->m_last_progress_update = helper->GetElapsedTime();
    
    bool proceed = helper->m_wrapper->OnProgress(helper->m_found_feasible);
    if (!proceed) {
      glp_ios_terminate(tree);
    }
  }
  
}

time_t TimetableOptimiserHelper::GetElapsedTime()
{
  time_t now = time(0);
  int elapsed = static_cast<int>(now - this->m_starttime);
  
  return elapsed;
}


TimetableOptimiserHelper::TimetableOptimiserHelper(TimetableOptimiser *wrapper) :
  m_starttime(time(0)), m_last_progress_update(0), m_found_feasible(false), m_wrapper(wrapper)
{
}

uint32 TimetableOptimiserHelper::ChangeTimetableArg (VehicleID v, uint order_number, bool wait_time) const {
	ModifyTimetableFlags mtf = wait_time ? MTF_WAIT_TIME : MTF_TRAVEL_TIME;
	return v | ((order_number - 1) << 20) | (mtf << 28);
}

SolveResult TimetableOptimiserHelper::Solve(const TimetableOptimiserParameters &optimisation_parms) {
  SolveResult result;
  
  glp_prob *prob = glp_create_prob();
  
  typedef std::map< std::pair< VehicleID, int >, int> VehicleOrderColumnMap;
  
  std::map<VehicleID, int>  timetable_start_column; // Column that holds the timetable start offset variable for the specified vehicle.
  VehicleOrderColumnMap travel_time_column; // Column that holds the travel time for the specified vehicle and order index.
  VehicleOrderColumnMap wait_time_column; // Column that holds the wait time for the specified vehicle and order index.
  
  // First pass: set-up the required structural variables and column constraints.
  for( TimetableMap::const_iterator v = m_current_timetables.begin(); v != m_current_timetables.end(); v++) {
    // Create a column for the start time of this vehicle's timetable
    int ncol = glp_add_cols(prob, 1);
    glp_set_col_kind(prob, ncol, GLP_IV);	// Integer column
    timetable_start_column[v->first] = ncol;
    glp_set_col_bnds(prob, ncol, GLP_DB, 0.0, optimisation_parms.period); // Set constraint boundaries for vehicle start time.
    
    // Set up a constraint to ensure that the entire timetable of this vehicle takes a multiple of TIMETABLE_PERIOD days.
    int total_time_row = glp_add_rows(prob, 1);
    glp_set_row_bnds(prob, total_time_row, GLP_FX, 0.0, 0.0);
    std::vector<int> total_time_indices;
    std::vector<double> total_time_values;
    
    // GLPK uses 1-based arrays, so we add a dummy element at index 0.
    total_time_indices.push_back(-1);
    total_time_values.push_back(0);
    
    // Create a modulo column for the total time constraint.
    int total_time_mod_col = glp_add_cols(prob, 1);
    glp_set_col_kind(prob, total_time_mod_col, GLP_IV);
    
    // Ensure that the timetable takes long enough to accommodate all vehicles.
    // Note: this will cause waiting times of more than TIMETABLE_PERIOD if there are too many
    // vehicles in the chain.
    glp_set_col_bnds(prob, total_time_mod_col, GLP_UP, -v->second.vehicle_count, -v->second.vehicle_count);
    
    // Minimize this modulo column so that the total length of the timetable is minimized.
    glp_set_obj_coef(prob, total_time_mod_col, -1.0);
    
    total_time_indices.push_back(total_time_mod_col);
    total_time_values.push_back(static_cast<double>(optimisation_parms.period));
    
    // For each movement
    for( MovementVector::const_iterator m = v->second.movements.begin(); m != v->second.movements.end(); m++) {
      // Create a column for the travel time of this particular order.
      int travelcol = glp_add_cols(prob, 1);
      glp_set_col_kind(prob, travelcol, GLP_IV);
      travel_time_column[VehicleOrderColumnMap::key_type(v->first, m->order_sequence)] = travelcol;
      glp_set_col_bnds(prob, travelcol, GLP_FX, m->travel_time, m->travel_time);
      
      // Create a column for the wait time.
      int waitcol = glp_add_cols(prob, 1);
      glp_set_col_kind(prob, waitcol, GLP_IV);
      wait_time_column[VehicleOrderColumnMap::key_type( v->first, m->order_sequence)] = waitcol;
      
      if( m->is_via_order ) {
	// No waiting
	glp_set_col_bnds(prob, waitcol, GLP_FX, 0, 0);
      } else {
	glp_set_col_bnds(prob, waitcol, GLP_LO, optimisation_parms.min_station_wait, 0);
	
	if ( ! m->is_end_of_line ) {  
	  // Optimize so that waiting on stations other than the end-of-line station is limited.
	  glp_set_obj_coef(prob, waitcol, 1.0);
	}
      }
      
      total_time_indices.push_back(travelcol);
      total_time_indices.push_back(waitcol);
      total_time_values.push_back(1);
      total_time_values.push_back(1);
    }
    
    glp_set_mat_row(prob, total_time_row, total_time_indices.size() -1, &total_time_indices[0], &total_time_values[0]);    
  }
  
  // Second pass: create row constraints to separate conflicting movements.
  for( TimetableMap::const_iterator v = m_current_timetables.begin(); v != m_current_timetables.end(); v++) {
    
    // For each movement
    for( MovementVector::const_iterator m = v->second.movements.begin(); m != v->second.movements.end(); m++) {
      // Check to see if there is an identical (conflicting) movement by another vehicle.
      // Examine only the movements of vehicles with a higher VehicleID. This ensures that we
      // don't get duplicate constraints.
      
      for(TimetableMap::const_iterator v2 = m_current_timetables.upper_bound(v->first); v2 != m_current_timetables.end(); v2++) {
	for ( MovementVector::const_iterator m2 = v2->second.movements.begin(); m2 != v2->second.movements.end(); m2++ ) {
	  if (m2->from == m->from && m2->to == m->to) {
	    // Movement m2 conflicts with m. Create constraint to ensure timetable separation.
	    // Do this for both arrival and departure
	    
	    for ( int pass = 0; pass < 2; pass++ ) {
	      bool departure = pass == 0;
	      bool arrival = !departure;
	      
	      int nrow = glp_add_rows(prob, 1);
	      
	      // Set row bounds: the allowable separation times between the two vehicles for this order (modulo TIMETABLE_PERIOD).
	      glp_set_row_bnds(prob, nrow, GLP_DB, optimisation_parms.min_separation, optimisation_parms.period - optimisation_parms.min_separation);
	      
	      std::vector<int> col_indices;
	      std::vector<double> col_values;
	      
	      // GLPK uses 1-based arrays, so we add a dummy element at index 0.
	      col_indices.push_back(-1);
	      col_values.push_back(0);
	      
	      col_indices.push_back(timetable_start_column[v->first]);	// Timetable start of first vehicle
	      col_values.push_back(1.0);
	      
	      col_indices.push_back(timetable_start_column[v2->first]);	// Timetable start of second vehicle
	      col_values.push_back(-1.0);
	      
	      for ( int i = 1; i <= m->order_sequence; i++ ) {
		if ( (i < m->order_sequence || arrival) && i > 1 ) {
		  // Travel time column for this order
		  col_indices.push_back( travel_time_column[VehicleOrderColumnMap::key_type(v->first, i)] ); // Travel time of first vehicle.
		  col_values.push_back(1.0);
		}
		
		if ( i < m->order_sequence ) {
		  // Wait time column for this order
		  col_indices.push_back( wait_time_column[VehicleOrderColumnMap::key_type(v->first, i)] );
		  col_values.push_back(1.0);
		}
	      }
	      
	      for ( int i = 1; i <= m2->order_sequence; i++ ) {
		if ( (i < m2->order_sequence || arrival) && i > 1 ) {
		  // Travel time columns
		  col_indices.push_back( travel_time_column[VehicleOrderColumnMap::key_type(v2->first, i)] ); // Travel time of second vehicle.
		  col_values.push_back(-1.0);
		}
		
		// Wait time column for this order
		if ( i < m2->order_sequence ) {
		  col_indices.push_back( wait_time_column[VehicleOrderColumnMap::key_type(v2->first, i)] );
		  col_values.push_back(-1.0);
		}
	      }
	      
	      // Create a modulo TIMETABLE_PERIOD column for the constraint
	      int modcol = glp_add_cols(prob, 1);
	      glp_set_col_kind(prob, modcol, GLP_IV);
	      col_indices.push_back(modcol);
	      col_values.push_back(optimisation_parms.period);
	      glp_set_col_bnds(prob, modcol, GLP_FR, 0.0, 0.0);
	      
	      // Set matrix values
	      glp_set_mat_row(prob, nrow, col_indices.size() -1, &col_indices[0], &col_values[0]);
	    }
	  }
	}
      }
    }
  }
  
  result = SolveResult::ERROR;
  
  glp_iocp parms;
  glp_init_iocp(&parms);
  
  parms.presolve = GLP_ON;
  parms.gmi_cuts = GLP_ON;
  parms.mir_cuts = GLP_ON;
  parms.cb_func = glpk_callback;
  parms.cb_info = static_cast<void*>(this);
  glp_intopt(prob, &parms);

  int mipstatus = glp_mip_status(prob);
  
  switch ( mipstatus ) {
    case GLP_OPT:
      result = SolveResult::OPTIMAL;
      break;
    case GLP_FEAS:
      result = SolveResult::FEASIBLE;
      break;
    case GLP_NOFEAS:
      result = SolveResult::INFEASIBLE;
      break;
  }

  
  glp_write_prob(prob, 0, "problem.lp");
  glp_write_mps(prob, GLP_MPS_FILE, NULL, "problem.mps");
  
  if ( result != SolveResult::ERROR ) {
    // Some degree of success was achieved. Copy the optimized timetable to the output variable.
    m_optimized_timetables.clear();
    
    for (TimetableMap::const_iterator vit = m_current_timetables.begin(); vit != m_current_timetables.end(); vit++) {
      VehicleID vehicle = vit->first;
      
      const Timetable &tt = vit->second;
      
      m_optimized_timetables[vehicle].start_offset = glp_mip_col_val(prob, timetable_start_column[vit->first]);
      
      int total_duration = 0;
      
      MovementVector temp_movements;
      for (MovementVector::const_iterator mit = vit->second.movements.begin(); mit != vit->second.movements.end(); mit++) { 
	Movement m = *mit;
	
	int wt_col = wait_time_column[VehicleOrderColumnMap::key_type(vehicle, m.order_sequence)];
	m.wait_time = glp_mip_col_val(prob, wt_col);
	total_duration += m.travel_time + m.wait_time;
	
	temp_movements.push_back(m);
      }
      
      // The GLPK solver sometimes returns a feasible but non-optimal solution. This solution
      // can contain excessive wait times at terminus stations. This is easy to fix post hoc.
      int desired_duration = optimisation_parms.period * tt.vehicle_count;
      
      if ( total_duration > desired_duration ) {
	  int excess_periods = ((total_duration - desired_duration) / optimisation_parms.period);
	  
	  for (MovementVector::iterator mit = temp_movements.begin(); mit != temp_movements.end(); mit++) { 
	    if ( mit->wait_time > optimisation_parms.period && excess_periods > 0 ) {
	      int strippable = CeilDiv(mit->wait_time, optimisation_parms.period) - 1;
	      int to_strip = strippable >= excess_periods ? excess_periods : strippable;

	      mit->wait_time -= optimisation_parms.period * to_strip;
	      total_duration -= optimisation_parms.period * to_strip;
	      excess_periods -= to_strip;
	    }
	  }
      }
      
      m_optimized_timetables[vehicle].movements = temp_movements;
      m_optimized_timetables[vehicle].vehicle_count = tt.vehicle_count;
      m_optimized_timetables[vehicle].total_duration = total_duration;
    }
  }
  
  glp_delete_prob(prob);
  
  return result;
}


void TimetableOptimiserHelper::RetrieveTimetables(VehicleType vtype, const VehicleListIdentifier *vlid) {
  m_current_timetables.clear();
  
  std::map<VehicleID, bool> seenVehicles;
  
  Vehicle *v;
  
  Group *group = NULL;
  if ( vlid != NULL) {
    group = Group::GetIfValid(vlid->index);
  }
  
  FOR_ALL_VEHICLES (v) {
    Vehicle *leader = v->First()->FirstShared();
    
    if ( seenVehicles.count(leader->index) != 0 ) {
	// We have already processed this vehicle.
	continue;
    }
    
    seenVehicles[leader->index] = true;
    
    if ( leader->type != vtype ) {
	continue;
    } else if ( group != NULL ) {
      // Check that at least one of the vehicles in the shared vehicle chain is in the group
      
      Vehicle *w = leader;
      bool skipVehicle = true;
      
      while ( w != NULL ) {
	if ( w->group_id == group->index ) {
	  skipVehicle = false;
	  break;
	}
	
	w = w->NextShared();
      }
      
      if ( skipVehicle == true ) {
	continue;
      }
    }
     
    OrderList *l = leader->orders.list;
    bool skip_vehicle = false;
    
    const Order *o = NULL;
    if ( l == NULL ) {
      skip_vehicle = true;
    } else {
      o = l->GetFirstOrder();
    }
    
    int order_count = 0;
    MovementVector temp_movements;
    
    while (!skip_vehicle) {
      if (o->IsConditional() || ! o->IsCompletelyTimetabled() ) {
	// Skip the vehicle alltogether
	skip_vehicle = true;
	continue;
      }

      // Ignore non-goto orders and implicit orders.
      if (!o->IsGotoOrder() || o->IsType(OT_IMPLICIT)) {
	// Ignore the order.
	continue;
      }
      
      // Find the previous goto orders
      const Order *from = o;
      do {
	from = l->GetPrevious(from);
	
	if ( o == from ) {	// Vehicle has insufficient orders.
	  skip_vehicle = true;
	  break;
	}
	
      } while (!from->IsGotoOrder() );
      
      if ( ! skip_vehicle ) {
	Movement m;
	m.travel_time = CeilDiv( o->travel_time, DAY_TICKS );
	m.order_id = o->index;
	m.order_sequence = ++order_count;
	m.wait_time = CeilDiv(o->wait_time, DAY_TICKS);
	m.to.type = GetDestinationType(o);
	m.to.id = o->GetDestination();
	m.from.type = GetDestinationType(from);
	m.from.id = from->GetDestination();

	if (
	  o->GetNonStopType() == OrderNonStopFlags::ONSF_NO_STOP_AT_ANY_STATION ||
	  o->GetNonStopType() == OrderNonStopFlags::ONSF_NO_STOP_AT_DESTINATION_STATION ||
	  m.to.type == DestinationType::DEPOT ||
	  m.to.type == DestinationType::WAYPOINT ) {
	    m.is_via_order = true;
	} else {
	  m.is_via_order = false;
	}
	
	temp_movements.push_back(m);
      }
      
      o = o->next;
      
      if ( o == NULL ) {
	break;
      }
    }
    
    if ( ! skip_vehicle ) {
      // Consider an order to be end of the line if:
      //   - The destination of the previous order was a depot.
      //   - The destination of the next order is the origin of the current order and both destinations are a station.

      int total_duration = 0;

      for ( MovementVector::iterator it = temp_movements.begin(); it != temp_movements.end(); it++) {
	MovementVector::iterator next;
	if ( it == temp_movements.end() - 1 ) {
	  next = temp_movements.begin();
	} else {
	  next = it + 1;
	}
	
	if ( it->to.type == DestinationType::STATION && it->from == next->to && next->to.type == DestinationType::STATION)
	  it->is_end_of_line = true; 
	else if ( it->from.type == DestinationType::DEPOT )
	  it->is_end_of_line = true;
	else 
	  it->is_end_of_line = false;
	
	total_duration = it->travel_time + it->wait_time;
      }

      // Record movements for real
      m_current_timetables[leader->index].movements = temp_movements;
      m_current_timetables[leader->index].start_offset = 0;
      m_current_timetables[leader->index].vehicle_count = l->GetNumVehicles();
      m_current_timetables[leader->index].total_duration = total_duration;
    }
  }
}

