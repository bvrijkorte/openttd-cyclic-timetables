#include "stdafx.h"

#include <utility>
#include <vector>
#include <map>

#include "command_func.h"
#include "date_func.h"
#include "vehicle_base.h"
#include "vehiclelist.h"
#include "timetable_optimiser.h"
#include "timetable_optimiser_helper.h"

// Default values
const int TimetableOptimiserParameters::TIMETABLE_PERIOD = 40;
const int TimetableOptimiserParameters::TIMETABLE_MIN_WAIT = 6;
const int TimetableOptimiserParameters::TIMETABLE_MIN_SEPARATION = 10;

TimetableOptimiserParameters::TimetableOptimiserParameters() :
  period(TIMETABLE_PERIOD),
  min_station_wait(TIMETABLE_MIN_WAIT),
  min_separation(TIMETABLE_MIN_SEPARATION)
{
  /* Empty */
}

TimetableOptimiserParameters::TimetableOptimiserParameters(const TimetableOptimiserParameters &other) {
  this->period = other.period;
  this->min_station_wait = other.min_station_wait;
  this->min_separation = other.min_separation;
}

TimetableOptimiserParameters::~TimetableOptimiserParameters() {
  /* Empty */
}

TimetableOptimiser::TimetableOptimiser(ProgressCallbackFn callback, const TimetableOptimiserParameters &optimisation_parms, void *param) :
  m_callback(callback), m_param(param), m_optimisation_parms(optimisation_parms) {
  m_helper = new TimetableOptimiserHelper(this);
}

TimetableOptimiser::TimetableOptimiser(const TimetableOptimiser &other) :
  m_callback(other.m_callback), m_param(other.m_param), m_optimisation_parms(other.m_optimisation_parms), m_helper ( new TimetableOptimiserHelper(* other.m_helper) ) {
  /* Copy constructor */
  m_helper->m_wrapper = this;
}


TimetableOptimiser::~TimetableOptimiser() {
  delete m_helper;
}

void TimetableOptimiser::RetrieveTimetables(VehicleType vtype, const VehicleListIdentifier *vlid) {
  m_helper->RetrieveTimetables(vtype, vlid);
}

void TimetableOptimiser::ApplyTimetables() {

  for (TimetableMap::const_iterator vit = m_helper->m_optimized_timetables.begin(); vit != m_helper->m_optimized_timetables.end(); vit++) {
    int total = 0;
    printf("Vehicle %d start offset: %d\n", vit->first, vit->second.start_offset);
    
    // Apply timetable offsets
    Vehicle *vehicle = Vehicle::Get( vit->first );
    int count = 0;
    do {
      DoCommandP(0, vehicle->index, _date + vit->second.start_offset + this->m_optimisation_parms.period * count, CMD_SET_TIMETABLE_START);
      count++;
    } while( (vehicle = vehicle->NextShared()) != NULL );
    
    for (MovementVector::const_iterator mit = vit->second.movements.begin(); mit != vit->second.movements.end(); mit++) {
      printf("Order %d: travel (from %d) for %d days and wait %d days at destination %d.\n", mit->order_sequence, mit->from.id, mit->travel_time, mit->wait_time, mit->to.id);
      total += mit->travel_time + mit->wait_time;
      
      DoCommandP(0, m_helper->ChangeTimetableArg(vit->first, mit->order_sequence, false), mit->travel_time * DAY_TICKS, CMD_CHANGE_TIMETABLE );
      DoCommandP(0, m_helper->ChangeTimetableArg(vit->first, mit->order_sequence, true), mit->wait_time * DAY_TICKS, CMD_CHANGE_TIMETABLE );
    }
    
    printf("Vehicle %d total time: %d\n", vit->first, total);
  }
}

bool TimetableOptimiser::OnProgress (bool feasible_found) {
  bool result = true;
  
  if ( m_callback ) {
     result = m_callback(feasible_found, m_param); 
  }
  
  return result;
}

SolveResult TimetableOptimiser::OptimiseTimetables()
{
  return m_helper->Solve(this->m_optimisation_parms);
};

int TimetableOptimiser::GetElapsedTime()
{
  return m_helper->GetElapsedTime();
}
