#ifndef TIMETABLE_OPTIMISER_H
#define TIMETABLE_OPTIMISER_H

enum SolveResult {
  OPTIMAL,      /// Success: An optimal timetable was calculated.
  FEASIBLE,     /// Success: A feasible timetable was calculated.
  INFEASIBLE, /// Failure: No cyclic timetable is possible within the given constraints.
  ERROR /// Failure: The optimizer failed to solve the problem (possibly because the problem is too complex).
};

class TimetableOptimiserHelper;
class VehicleListIdentifier;

struct TimetableOptimiserParameters {
  TimetableOptimiserParameters(const TimetableOptimiserParameters &other);
  TimetableOptimiserParameters();
  virtual ~TimetableOptimiserParameters();
  
  int period;
  int min_station_wait;
  int min_separation;
  
  private:
    static const int TIMETABLE_MIN_WAIT;
    static const int TIMETABLE_MIN_SEPARATION;
    static const int TIMETABLE_PERIOD;
};

typedef bool (*ProgressCallbackFn) (bool feasible_found, void *param);

class TimetableOptimiser {
  public:
    TimetableOptimiser(ProgressCallbackFn callback, const TimetableOptimiserParameters &optimisation_parms, void *param);
    TimetableOptimiser(const TimetableOptimiser &other);
    virtual ~TimetableOptimiser();

    void RetrieveTimetables(VehicleType vtype, const VehicleListIdentifier *vlid = NULL);
    SolveResult OptimiseTimetables();
    void ApplyTimetables();
  
    /** 
     * Callback function used by the solver. Provides the
     * running time in seconds as well as whether a feasible
     * (but non-optimal) solution was found already.
     * 
     * The function should return true if the search must continue
     * or false to cancel it.
     */
    bool OnProgress (bool feasible_found);
  
    int GetElapsedTime();
    
    
    
  private:
    ProgressCallbackFn m_callback;
    void* m_param;
    TimetableOptimiserParameters m_optimisation_parms;
    TimetableOptimiserHelper *m_helper;
    
    friend class TimetableOptimiserHelper;
};


#endif
