#ifndef TIMETABLE_OPTIMISER_GUI_H
#define TIMETABLE_OPTIMISER_GUI_H

void ShowTimetableOptimiser(VehicleType vtype);
void ShowTimetableOptimiser(const VehicleListIdentifier &vlid);

class TimetableOptimiserWindow;
class TimetableOptimiser;
class ThreadMutex;
class ThreadObject;
class TimetableOptimiserParameters;

struct TimetableProgressWindow : public Window {
    int m_runtime;
    bool m_feasible_found;
    ThreadMutex *m_optimiser_status_mutex;
    TimetableOptimiserWindow *m_parent;
    bool m_optimiser_done;
    bool m_optimiser_successful;
    bool m_optimiser_optimal;
    bool m_must_cancel;
    bool m_querying;

    TimetableProgressWindow(const WindowDesc *desc, TimetableOptimiserWindow *parent);
    virtual ~TimetableProgressWindow();
    
    virtual void Init(const WindowDesc *desc);
    virtual void DrawWidget(const Rect& r, int widget) const;

    void OnOptimisationComplete (bool successful, bool optimal);
    
    virtual void OnClick(Point pt, int widget, int click_count);
    bool UpdateProgress(bool feasible_found);
    
    void Cancel();
    
    virtual void OnHundredthTick();
};

struct TimetableOptimiserWindow : public Window {
  public:
    ThreadObject *thread;
    const VehicleListIdentifier *vlid;
    VehicleType vtype;
    TimetableProgressWindow *progress_window;
    TimetableOptimiserParameters *optimiser_parms;
    TimetableOptimiser *optimiser;
    
    TimetableOptimiserWindow(const WindowDesc *desc, VehicleType vtype);
    TimetableOptimiserWindow(const WindowDesc *desc, const VehicleListIdentifier &vlid);
    
    virtual ~TimetableOptimiserWindow();
    
    void OnOptimisationDone();
    void Execute();
    virtual void OnClick(Point pt, int widget, int click_count);
    virtual void SetStringParameters(int widget) const;
};

#endif // TIMETABLE_OPTIMISER_GUI_H
